#ifndef FASERACTSKALMANFILTER_TRACKCLASSIFICATION_H
#define FASERACTSKALMANFILTER_TRACKCLASSIFICATION_H

#include "FaserActsRecMultiTrajectory.h"
#include "TrackerSimData/TrackerSimDataCollection.h"

struct ParticleHitCount {
  int particleId;
  size_t hitCount;
};

/// Identify all particles that contribute to a trajectory.
void identifyContributingParticles(
    const TrackerSimDataCollection& simDataCollection,
    const FaserActsRecMultiTrajectory& trajectories, size_t tip,
    std::vector<ParticleHitCount>& particleHitCounts);

void identifyContributingParticles(
    const TrackerSimDataCollection& simDataCollection,
    const std::vector<const Tracker::FaserSCT_Cluster*> clusters,
    std::vector<ParticleHitCount>& particleHitCounts);

#endif  // FASERACTSKALMANFILTER_TRACKCLASSIFICATION_H
