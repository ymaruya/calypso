// Local include(s):
#include "xAODFaserTruth/FaserTruthEventAuxContainer.h"

namespace xAOD {

   FaserTruthEventAuxContainer::FaserTruthEventAuxContainer()
      : AuxContainerBase() {

      AUX_VARIABLE( signalProcessVertexLink );

      AUX_VARIABLE( weights );

      AUX_VARIABLE( crossSection );
      AUX_VARIABLE( crossSectionError );

      AUX_VARIABLE( truthVertexLinks );
      AUX_VARIABLE( truthParticleLinks );
      
      AUX_VARIABLE( weightNames );
      AUX_VARIABLE( mcChannelNumber );
   }

} // namespace xAOD
