#ifndef XAODFASERTRUTH_TRUTHEVENT_H
#define XAODFASERTRUTH_TRUTHEVENT_H

// System include(s):
#include <utility>
#include <vector>

// EDM include(s):
#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"
#include "xAODBase/ObjectType.h"
#include "xAODFaserBase/FaserObjectType.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthParticleContainer.h"
#include "xAODFaserTruth/FaserTruthVertexContainer.h"

namespace xAOD {

   /// Class describing a signal truth event in the MC record
   ///
   class FaserTruthEvent : public SG::AuxElement {

   public:
      /// Default constructor
      FaserTruthEvent();
      /// Virtual destructor, to make vtable happy
      virtual ~FaserTruthEvent() {}

      /// @name Access to all the particles associated with the event
      /// @{

      /// Type of a single truth particle link
      typedef ElementLink< FaserTruthParticleContainer > FaserTruthParticleLink_t;
      /// Type of the truth particle links vector
      typedef std::vector< FaserTruthParticleLink_t > FaserTruthParticleLinks_t;

      /// Get all the truth particles
      const FaserTruthParticleLinks_t& truthParticleLinks() const;
      /// Set all the truth particles
      void setFaserTruthParticleLinks( const FaserTruthParticleLinks_t& plinks );
      /// Get the number of truth particles
      size_t nFaserTruthParticles() const;
      /// Get the link to one of the truth particles
      const FaserTruthParticleLink_t& truthParticleLink( size_t index ) const;
      /// Get a pointer to one of the truth particles
      const FaserTruthParticle* truthParticle( size_t index ) const;
      /// Add one truth particle
      void addFaserTruthParticleLink( const FaserTruthParticleLink_t& plink );
      /// Remove all truth particles
      void clearFaserTruthParticleLinks();

      /// @}
      
      /// @name Access to all the vertices associated with the event
      /// @{

      /// Type of a single truth vertex link
      typedef ElementLink< FaserTruthVertexContainer > FaserTruthVertexLink_t;
      /// Type of the truth particle links vector
      typedef std::vector< FaserTruthVertexLink_t > FaserTruthVertexLinks_t;

      /// Get all the truth vertices
      const FaserTruthVertexLinks_t& truthVertexLinks() const;
      /// Set all the truth vertices
      void setFaserTruthVertexLinks( const FaserTruthVertexLinks_t& links );
      /// Get the number of truth vertices
      size_t nTruthVertices() const;
      /// Get the link to one of the truth vertices
      const FaserTruthVertexLink_t& truthVertexLink( size_t index ) const;
      /// Get a pointer to one of the truth vertices
      const FaserTruthVertex* truthVertex( size_t index ) const;
      /// Add one truth vertex
      void addFaserTruthVertexLink( const FaserTruthVertexLink_t& vlink );
      /// Remove all truth vertices
      void clearFaserTruthVertexLinks();

      /// @}
      
      /// @name Simple event properties
      /// @{

      /// Const access to the weights vector
      /// @todo Need to add the map-like interface for the weights: very important!
      const std::vector< float >& weights() const;
      /// Set the event weights
      /// @todo Need to add named weight access: vector<string>
      void setWeights( const std::vector< float >& weights );

      /// Get the cross section
      float crossSection() const;
      /// Set the cross-section
      void setCrossSection( float value );

      /// Get the cross section error
      float crossSectionError() const;
      /// Set the cross-section error
      void setCrossSectionError( float value );

      /// Set the cross-section and its error
      void setCrossSection( float value, float error );

      /// @}

      /// @name Parton density info
      ///
      /// Optional due to particle gun & NLO events where a single PDF info
      /// doesn't work.
      ///
      /// @{

      /// Accessor enums for PDF info parameter lookup
      enum PdfParam {
         PDGID1 = 0, ///< [int]
         PDGID2 = 1, ///< [int]
         PDFID1 = 2, ///< [int]
         PDFID2 = 3, ///< [int]
         X1 = 4,     ///< [float]
         X2 = 5,     ///< [float]
         SCALE = 6,  ///< Not implemented!!!
         Q = 6,      ///< [float]
         PDF1 = 7,   ///< Not implemented!!!
         PDF2 = 8,   ///< Not implemented!!!
         XF1 = 7,    ///< [float]
         XF2 = 8     ///< [float]
      }; // enum PdfParam

      /// Read an integer PDF info parameter
      bool pdfInfoParameter( int& value, PdfParam parameter ) const;
      /// Read a floating point PDF info parameter
      bool pdfInfoParameter( float& value, PdfParam parameter ) const;

      /// Set an integer PDF info parameter
      bool setPdfInfoParameter( int value, PdfParam parameter );
      /// Set a floating point PDF info parameter
      bool setPdfInfoParameter( float value, PdfParam parameter );

      /// Helper struct holding a full set of PDF info values
      struct PdfInfo {

         /// Constructor to set (invalid) defaults
         PdfInfo();

         /// Check if all the variables in the object are valid
         bool valid() const;

         int pdgId1;
         int pdgId2;
         int pdfId1;
         int pdfId2;
         float x1;
         float x2;
         float Q;
         float xf1;
         float xf2;

      }; // struct PdfInfo

      /// Retrieve a full PdfInfo with a single call
      /// @note May have invalid values -- use valid() to check.
      PdfInfo pdfInfo() const;

      /// @}

      /// @name Links to particles and vertices in the event
      /// @{

      /// Pointer to a vertex representing the primary interaction point
      ///
      /// The naming of the function is a bit misleading. The returned vertex
      /// can only be interpreted as an interaction *position*.
      ///
      const FaserTruthVertex* signalProcessVertex() const;
      /// Link to the vertex representing the primary interaction point
      const FaserTruthVertexLink_t& signalProcessVertexLink() const;
      /// Set pointer to a vertex representing the primary interaction point
      void setSignalProcessVertexLink( const FaserTruthVertexLink_t& link );

      /// Pointer to the incoming primary particle
      const FaserTruthParticle* primaryParticle() const;

      /// Get the link to the primary particle
      const FaserTruthParticleLink_t& primaryParticleLink() const;

      /// Set incoming primary particle
      void setPrimaryParticleLink( const FaserTruthParticleLink_t& pcl);

      /// @}
      /// The type for the IParticle, this should not be used
      virtual Type::ObjectType type() const;
      
      /// The type of the object as a simple enumeration
      FaserType::ObjectType faserType() const;

      /// @name Simple truth meta data properties 
      /// @{

      uint32_t mcChannelNumber() const;
      void setMcChannelNumber( uint32_t value );

      const std::vector< std::string >& weightNames() const;
      void setWeightNames( const std::vector< std::string >& value );

      /// @}

      /// Function making sure that the object is ready for persistification
      void toPersistent();

   }; // class FaserTruthEvent

} // namespace xAOD

#endif // XAODFASERTRUTH_TRUTHEVENT_H
