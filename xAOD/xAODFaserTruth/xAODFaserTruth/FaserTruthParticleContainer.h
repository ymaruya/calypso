// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: FaserTruthParticleContainer.h 622193 2014-10-16 16:08:34Z krasznaa $
#ifndef XAODFASERTRUTH_TRUTHPARTICLECONTAINER_H
#define XAODFASERTRUTH_TRUTHPARTICLECONTAINER_H

// Local include(s):
#include "xAODFaserTruth/FaserTruthParticle.h"
// EDM include(s):
#include "AthContainers/DataVector.h"


namespace xAOD {
   // Alias
   typedef DataVector< FaserTruthParticle > FaserTruthParticleContainer;
}

// Declare a CLID for the class for Athena:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::FaserTruthParticleContainer, 1237340784, 1 )

#endif // XAODFASERTRUTH_TRUTHPARTICLECONTAINER_H
